//Open Add/Edit Modal
$(document).on("click", ".open-modal-edit", function () {
    var id = $(this).data('id');

    // Clear previous messages
    $(".print-success-msg").empty().hide();
    $(".print-error-msg").hide();

    if (!id) {
        // Add new record
        $("#modal-edit-title").text("Add new record");
        $(":input").not("input[name='_token']").val("");
    } else {
        // Edit record
        $("#modal-edit-title").text("Edit record");
        $("#edit-id").val( id );

        $("input[name='provider']").val($("#table-row-" + id).children('.table-provider').text());
        $("input[name='brand_label']").val($("#table-row-" + id).children('.table-brand_label').text());
        $("input[name='location']").val($("#table-row-" + id).children('.table-location').text());
        $("input[name='cpu']").val($("#table-row-" + id).children('.table-cpu').text());
        $("input[name='drive_label']").val($("#table-row-" + id).children('.table-drive_label').text());
        $("input[name='price']").val($("#table-row-" + id).children('.table-price').text());
        $("input[name='id']").val(id);

    }
    $("#modal-edit").modal("show");
})

// Save Button
$("#modal-edit-save").click(function(e){
    var _token = $("input[name='_token']").val();
    var provider = $("input[name='provider']").val();
    var brand_label = $("input[name='brand_label']").val();
    var location = $("input[name='location']").val();
    var cpu = $("input[name='cpu']").val();
    var drive_label = $("input[name='drive_label']").val();
    var price = $("input[name='price']").val();
    var id = $("input[name='id']").val();


    $.ajax({
        url: "/admin/create",
        type:'POST',
        data: {_token:_token, provider:provider, brand_label:brand_label, location:location,
            cpu:cpu, drive_label:drive_label, price:price, id:id},
        success: function(data) {
            if($.isEmptyObject(data.error)){
                $(".print-success-msg").text(data.success).show();
                $(".print-error-msg").hide();

                if (!id) {
                    // New record
                    var count = parseInt($("#count-items").text());
                    $("#count-items").text(++count);
                } else {
                    // Update table
                    $("#table-row-" + id).children('.table-provider').text(provider);
                    $("#table-row-" + id).children('.table-location').text(location);
                    $("#table-row-" + id).children('.table-cpu').text(cpu);
                    $("#table-row-" + id).children('.table-drive_label').text(drive_label);
                    $("#table-row-" + id).children('.table-price').text(price);
                    $("#table-row-" + id).children('.table-brand_label').text(brand_label);
                }

                setTimeout(function () {
                    $("#modal-edit").modal("hide");
                }, 2000);
            }else{
                printErrorMsg(data.error);
            }
        }
    });
});


function printErrorMsg (msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

// Delete Modal
$(document).on("click", ".open-modal-delete", function () {
    var id = $(this).data("id");
    $("#modal-delete-btn").data("id", id);

    // Clear previous messages
    $(".print-success-delete-msg").empty().hide();
    $(".print-error-delete-msg").hide();

    $("#modal-delete").modal("show");
})

//Delete Button
$(document).on("click", "#modal-delete-btn", function () {
    var _token = $("input[name='_token']").val();
    var id = $(this).data("id");

    $.ajax({
        url: "/admin/delete",
        type:'POST',
        data: {_token:_token, id:id},
        success: function(data) {
            if($.isEmptyObject(data.error)){
                var count = parseInt($("#count-items").text());
                $(".print-success-delete-msg").text(data.success).show();
                $(".print-error-delete-msg").hide();

                $("#table-row-" + id ).empty();

                $("#count-items").text(--count);

                setTimeout(function () {
                    $("#modal-delete").modal("hide");
                }, 2000);
            }else{
                $(".print-error-delete-msg").text(data.error).show();
            }
        }
    });
})

<form>
    @foreach($model as $row)
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">{{ $row }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="" placeholder="Email">
            </div>
        </div>
    @endforeach

    <div class="form-group row">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Sign in</button>
        </div>
    </div>
</form>

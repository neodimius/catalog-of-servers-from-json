@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Admin Panel</h1>
        <h2><span id="count-items">{{ $count }}</span> items</h2>

        <div class="d-flex justify-content-between">
            <button class="btn btn-success btn-lg open-modal-edit">Add new</button>
            <a href="{{ url('/admin') }}" class="btn btn-warning btn-lg">Refresh table</a>
            <a href="{{ url('/admin/update-catalog-from-server') }}" class="btn btn-danger btn-lg">Sync Data with Json file</a>
        </div>

        @if($message)
            <div class="alert alert-success">
                {{ $message }}
            </div>
        @endif

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">
                    <a href="{{ url('admin/?order=provider&sort='
                                    . (($order === 'provider' && $sort === 'asc') ? 'desc' : 'asc')) }}">Provider
                    @if ($order === 'provider')
                        @if ($sort === 'asc')
                            &#x2191;
                        @else
                            &#x2193;
                        @endif
                    @endif
                    </a>
                </th>
                <th scope="col">
                    <a href="{{ url('admin/?order=brand_label&sort='
                                    . (($order === 'brand_label' && $sort === 'asc') ? 'desc' : 'asc')) }}">Brand
                        @if ($order === 'brand_label')
                            @if ($sort === 'asc')
                                &#x2191;
                            @else
                                &#x2193;
                            @endif
                        @endif
                    </a>
                </th>
                <th scope="col">
                    <a href="{{ url('admin/?order=location&sort='
                                    . (($order === 'location' && $sort === 'asc') ? 'desc' : 'asc')) }}">Location
                        @if ($order === 'location')
                            @if ($sort === 'asc')
                                &#x2191;
                            @else
                                &#x2193;
                            @endif
                        @endif
                    </a>
                </th>
                <th scope="col">
                    <a href="{{ url('admin/?order=cpu&sort='
                                    . (($order === 'cpu' && $sort === 'asc') ? 'desc' : 'asc')) }}">CPU
                        @if ($order === 'cpu')
                            @if ($sort === 'asc')
                                &#x2191;
                            @else
                                &#x2193;
                            @endif
                        @endif
                    </a>
                </th>
                <th scope="col">
                    <a href="{{ url('admin/?order=drive_label&sort='
                                    . (($order === 'drive_label' && $sort === 'asc') ? 'desc' : 'asc')) }}">Drive
                        @if ($order === 'drive_label')
                            @if ($sort === 'asc')
                                &#x2191;
                            @else
                                &#x2193;
                            @endif
                        @endif
                    </a>
                </th>
                <th scope="col">
                    <a href="{{ url('admin/?order=price&sort='
                                    . (($order === 'price' && $sort === 'asc') ? 'desc' : 'asc')) }}">Price
                        @if ($order === 'price')
                            @if ($sort === 'asc')
                                &#x2191;
                            @else
                                &#x2193;
                            @endif
                        @endif
                    </a>
                </th>
                <th scope="col">
                    Options
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $row)
                <tr id="table-row-{{ $row->id }}">
                    <td class="table-provider">{{ $row->provider }}</td>
                    <td class="table-brand_label">{{ $row->brand_label }}</td>
                    <td class="table-location">{{ $row->location }}</td>
                    <td class="table-cpu">{{ $row->cpu }}</td>
                    <td class="table-drive_label">{{ $row->drive_label }}</td>
                    <td class="table-price">{{ $row->price }}</td>
                    <td>
                        <button href="" class="btn btn-outline-success btn-small open-modal-edit"  data-id="{{ $row->id }}">Edit</button>
                        <button href="" class="btn btn-outline-danger btn-small open-modal-delete"  data-id="{{ $row->id }}">Delete</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
    <div class="container p-0 d-flex justify-content-center">
        <?= $data->links(); ?>
    </div>

    <!-- Modal-EDIT -->
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content form-elegant">
                <!--Header-->
                <div class="modal-header text-center">
                    <h2 id="modal-edit-title"></h2>
                </div>
                <!--Body-->
                <div class="alert alert-success print-success-msg" style="display:none"></div>
                <div class="alert alert-danger print-error-msg" style="display:none">
                    <ul></ul>
                </div>

                <div class="modal-body mx-4">
                    <!--Body-->
                    <div class="form-group row">
                        <label data-error="wrong" data-success="right" for="Form-provider" class="col-sm-2 col-form-label">Provider</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="provider" id="provider" placeholder="provider">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label data-error="wrong" data-success="right" for="brand_label" class="col-sm-2 col-form-label">Brand</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="brand_label" id="brand_label" placeholder="brand_label">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label data-error="wrong" data-success="right" for="location" class="col-sm-2 col-form-label">location</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="location" id="location" placeholder="Location">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label data-error="wrong" data-success="right" for="cpu" class="col-sm-2 col-form-label">CPU</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cpu" id="cpu" placeholder="CPU">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label data-error="wrong" data-success="right" for="drive_label" class="col-sm-2 col-form-label">Drive</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="drive_label" id="drive_label" placeholder="Drive">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label data-error="wrong" data-success="right" for="price" class="col-sm-2 col-form-label">Price</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="price" id="price" placeholder="Price">
                        </div>
                    </div>

                    <input type="hidden" name="id">

                </div>
                <!--Footer-->
                <div class="modal-footer mx-5 pt-3 mb-1">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="modal-edit-save">Save</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Modal-Edit-END -->

    <!-- Modal-Delete -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content form-elegant">
                <!--Header-->
                <div class="alert alert-success print-success-delete-msg" style="display:none"></div>
                <div class="alert alert-danger print-error-delete-msg" style="display:none">
                    <ul></ul>
                </div>
                <div class="modal-header text-center">
                    <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3">Are you sure you want to delete the record?</h3>
                </div>
                <!--Footer-->
                <div class="modal-footer mx-5 pt-3 mb-1">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" data-id="" id="modal-delete-btn">Delete</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Modal -->

    <script src="{{ asset('js/catalog.js') }}?v={{ time() }}" defer></script>
@endsection

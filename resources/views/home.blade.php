@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center align-items-center flex-column">
        <h1>Catalog of servers</h1>
    @guest
        <h3>please <a href="{{ route('login') }}">log in</a> for edit table</h3>
    @else
        <a class="btn btn-lg btn-secondary justify-content-center" href="{{ url('admin') }}">Admin Panel</a>
    @endguest
    </div>

    <h2>{{ $count }} items</h2>

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">
                <a href="{{ url('/?order=provider&sort='
                                    . (($order === 'provider' && $sort === 'asc') ? 'desc' : 'asc')) }}">Provider
                    @if ($order === 'provider')
                        @if ($sort === 'asc')
                            &#x2191;
                        @else
                            &#x2193;
                        @endif
                    @endif
                </a>
            </th>
            <th scope="col">
                <a href="{{ url('/?order=brand_label&sort='
                                    . (($order === 'brand_label' && $sort === 'asc') ? 'desc' : 'asc')) }}">Brand
                    @if ($order === 'brand_label')
                        @if ($sort === 'asc')
                            &#x2191;
                        @else
                            &#x2193;
                        @endif
                    @endif
                </a>
            </th>
            <th scope="col">
                <a href="{{ url('/?order=location&sort='
                                    . (($order === 'location' && $sort === 'asc') ? 'desc' : 'asc')) }}">Location
                    @if ($order === 'location')
                        @if ($sort === 'asc')
                            &#x2191;
                        @else
                            &#x2193;
                        @endif
                    @endif
                </a>
            </th>
            <th scope="col">
                <a href="{{ url('/?order=cpu&sort='
                                    . (($order === 'cpu' && $sort === 'asc') ? 'desc' : 'asc')) }}">CPU
                    @if ($order === 'cpu')
                        @if ($sort === 'asc')
                            &#x2191;
                        @else
                            &#x2193;
                        @endif
                    @endif
                </a>
            </th>
            <th scope="col">
                <a href="{{ url('/?order=drive_label&sort='
                                    . (($order === 'drive_label' && $sort === 'asc') ? 'desc' : 'asc')) }}">Drive
                    @if ($order === 'drive_label')
                        @if ($sort === 'asc')
                            &#x2191;
                        @else
                            &#x2193;
                        @endif
                    @endif
                </a>
            </th>
            <th scope="col">
                <a href="{{ url('/?order=price&sort='
                                    . (($order === 'price' && $sort === 'asc') ? 'desc' : 'asc')) }}">Price
                    @if ($order === 'price')
                        @if ($sort === 'asc')
                            &#x2191;
                        @else
                            &#x2193;
                        @endif
                    @endif
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $row)
            <tr id="table-row-{{ $row->id }}">
                <td class="table-provider">{{ $row->provider }}</td>
                <td class="table-brand_label">{{ $row->brand_label }}</td>
                <td class="table-location">{{ $row->location }}</td>
                <td class="table-cpu">{{ $row->cpu }}</td>
                <td class="table-drive_label">{{ $row->drive_label }}</td>
                <td class="table-price">{{ $row->price }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

{{--    Pagination links--}}
    <div class="container p-0 d-flex justify-content-center">
        <?= $data->links(); ?>
    </div>
</div>
@endsection

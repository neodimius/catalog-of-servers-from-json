<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/admin', 'AdminController@index');
Route::post('/admin/create', 'AdminController@create');
Route::post('/admin/delete', 'AdminController@delete');
Route::get('/admin/edit/{id}', 'AdminController@create');
Route::get('/admin/update-catalog-from-server', 'AdminController@updateCatalogFromServer');

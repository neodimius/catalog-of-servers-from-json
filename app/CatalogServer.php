<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CatalogServer extends Model
{
    private $resource = 'https://old.my.inxy.com/json/servers_catalog.json';
    protected $fillable = ['name','provider', 'brand_label', 'location', 'cpu', 'drive_label', 'price'];
    public $name;
    public $provider;
    public $brand_label;
    public $location;
    public $cpu;
    public $drive_label;
    public $price;

    /**
     * Update database from the json on the server
     *
     * @return string
     */
    public function updateCatalogFromServer(): string
    {
        $prepareJsonData = array();
        $jsonArr = json_decode(file_get_contents($this->resource), true);
        $countDeleted = 0;

        $dbData = DB::table('catalog_servers')->get();

        // Prepare json data:
        foreach ($jsonArr['data'] as $row) {
            $name = $this->generateName($row);

            $prepareJsonData[$name] = [
                'name' => $name,
                'provider' => $row['provider'],
                'brand_label' => $row['brand_label'],
                'location' => $row['location'],
                'cpu' => $row['cpu'],
                'drive_label' => $row['drive_label'],
                'price' => $row['price'],
            ];
        }

        // If database not empty
        if (count($dbData) > 0) {
            foreach ($dbData as $row) {
                if (array_key_exists($row->name, $prepareJsonData)) {
                    // Check if record in the DB
                    unset($prepareJsonData[$row->name]);
                } else {
                    // Remove records in the DB
                    DB::table('catalog_servers')->where('name', $row->name)->delete();
                    $countDeleted++;
                }
            }
        }

        // Update database
        self::insert($prepareJsonData);

        // Cook message
        $countInserted = count($prepareJsonData);
        $message = "Sync done! ";
        if (!$countInserted && !$countDeleted) {
            $message .= 'Data was not changed. ';
        } else {
            $message .= $countInserted ?: '0';
            $message .= ' records was inserted. ';
            $message .= $countDeleted ?: '0';
            $message .= ' records was deleted. ';
        }

        return $message;
    }

    /**
     * Generate a unique name for the product
     *
     * @param array $row
     * @return string
     */
    public function generateName(array $row): string
    {
        return  $row['provider'] . '_'
            . $row['brand_label'] . '_'
            . $row['location'] . '_'
            . $row['cpu'] . '_'
            . $row['drive_label'] . '_'
            . $row['price'];
    }
}

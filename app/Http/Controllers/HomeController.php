<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show main page
     *
     * @param Request $request
     * @return Renderable
     */
    public function index(Request $request)
    {
        $order = $request->input('order') ?: 'provider';
        $sort = false;
        $data = DB::table('catalog_servers');
        $count = $data->count();

        // Sorting
        $sort = $request->input('sort') ?: false;
        $data->orderBy($order, $sort ?: 'desc');

        //Pagination
        $data = $data->paginate(10);

        return view('home', [
            'data' => $data->appends(request()->query()),
            'count' => $count,
            'order' => $order,
            'sort' => $sort,
        ]);
    }
}

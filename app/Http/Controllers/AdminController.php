<?php

namespace App\Http\Controllers;

use App\CatalogServer;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Validator;

class AdminController extends Controller
{
    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $order = $request->input('order') ?: 'provider';
        $sort = false;
        $data = DB::table('catalog_servers');
        $count = $data->count();

        $message = $request->query('message') ?: false;

        // Sorting
        $sort = $request->input('sort') ?: false;
        $data->orderBy($order, $sort ?: 'desc');

        //Pagination
        $data = $data->paginate(10);

        return view('admin.index', [
            'data' => $data->appends(request()->query()),
            'count' => $count,
            'order' => $order,
            'sort' => $sort,
            'message' => $message
        ]);
    }

    /**
     * Create record
     *
     * @param Request $request
     * @param bool $id
     * @return JsonResponse
     */
    public function create(Request $request, $id = false): JsonResponse
    {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'provider' => 'required',
            'brand_label' => 'required',
            'location' => 'required',
            'cpu' => 'required',
            'drive_label' => 'required',
            'price' => 'required',
        ]);


        if ($validator->passes()) {
            $model = new CatalogServer();
            $inputs['name'] = $model->generateName($inputs);

            if($inputs['id']) {
                //Update record
                unset($inputs->id);
                $model->find($inputs['id'])->update($inputs);

                return response()->json(['success'=>'Edited record successful.']);
            }

            // Create new record
            unset($inputs->id);
            $model->create($inputs);

            return response()->json(['success'=>'Added new record successful.']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Delete record
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'id' => 'required',
        ]);

        if ($validator->passes()) {
            $result = DB::table('catalog_servers')->where('id', $inputs['id'])->delete();

            if ($result) {
                return response()->json(['success'=>'Removed record successful.']);
            }
            return response()->json(['error'=> 'the record has not been deleted because it does not exist.']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     *
     * @return RedirectResponse
     */
    public function updateCatalogFromServer(): RedirectResponse
    {
        $model = new CatalogServer();

        return redirect()->action(
            'AdminController@index', ['message' => $model->updateCatalogFromServer()]
        );
    }
}

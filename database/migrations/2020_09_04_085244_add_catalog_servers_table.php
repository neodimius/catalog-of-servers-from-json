<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCatalogServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('provider');
            $table->string('brand_label');
            $table->string('location');
            $table->string('cpu');
            $table->string('drive_label');
            $table->integer('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_servers');
    }
}
